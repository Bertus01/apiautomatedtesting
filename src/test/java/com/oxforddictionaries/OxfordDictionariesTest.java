package com.oxforddictionaries;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ResponseBody;
import com.jayway.restassured.specification.RequestSpecification;
import groovy.json.JsonException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;


public class OxfordDictionariesTest extends BaseOxfordDictionariesTest {


    @BeforeClass
    public  static void setup() {
        RestAssured.useRelaxedHTTPSValidation();
    }

    @Test
    public void invalidWordForOxfordDictionaryAPIRespondsWith404() throws JsonException {

            given().header("Accept", "application/json")
                    .header("app_id", app_id_value)
                    .header("app_key", app_key_value)
                    .when()
                    .get(OXFORD_DICTIONARY_REQUEST_MAPPING+"hellwwwo")
                    .then().statusCode(404);

    }
    @Test
    public void validWordForOxfordDictionaryAPIRespondsWith200() throws JsonException {

        given().header("Accept", "application/json")
                .header("app_id", app_id_value)
                .header("app_key", app_key_value)
                .when()
                .get(OXFORD_DICTIONARY_REQUEST_MAPPING+"hello")
                .then().statusCode(200);


    }
    @Test
    public void validateResponseJSONBodyContainsDefinitions() throws JsonException {
//
        RequestSpecification httpRequest = RestAssured.given();
        // Set HTTP Headers
        httpRequest.header("Accept", "application/json");
        httpRequest.header("app_id", app_id_value);
        httpRequest.header("app_key", app_key_value);

        Response response = httpRequest.get(OXFORD_DICTIONARY_REQUEST_MAPPING+"swimming");

        // Get Response Body
        ResponseBody body = response.getBody();

        // Get Response Body as String
        String bodyStringValue = body.asString();
        System.out.println(bodyStringValue);
        // Validate if Response Body Contains definition String
        Assert.assertTrue(bodyStringValue.contains("definitions"));

    }


}
